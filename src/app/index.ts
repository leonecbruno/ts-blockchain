import * as express from "express";
import Blockchain from "../blockchain";
import Wallet from "../wallet";
import TransactionPool from "../wallet/transaction-pool";

const app = express();
const blockchain = new Blockchain();
const wallet = new Wallet();
const tp: TransactionPool = new TransactionPool();
const p2pServer: P2pServer