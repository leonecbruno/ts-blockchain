import { Transaction } from "../wallet/transaction";
import { TransactionInput } from "../wallet/transaction-input";
import * as config from "../config";

export default class Block {
    timestamp: number;
    lastHash: string;
    hash: string;
    //for cryptocurrency - transactions are the data in each block
    //but the blockchain can hold any data
    data: any;
    nonce: number;
    difficulty: number;

    constructor(timestamp: number, lastHash: string, hash: string, data: any, nonce: number, difficulty: number) {
        this.timestamp = timestamp;
        this.lastHash = lastHash;
        this.hash = hash;
        this.data = data;
        this.nonce = nonce;
        this.difficulty = difficulty;
    }

    /**
     * Primeiro bloco do blockchain (Bloco de genesis)
     */
    static getGenesisBlock(): Block {
        let genesisTx: Transaction = new Transaction();
        genesisTx.id = 'genesis';
        genesisTx.txInput = new TransactionInput(0, '-----');
        return new Block(0, '-----', 'f1r5t ha4h', genesisTx.txInput, 0, config.DIFFICULTY);
    }

    toString(): string {
        return `Block -
            Timestamp: ${this.timestamp}
            Last Hash: ${this.lastHash.substring(0, 10)}
            Hash     : ${this.hash.substring(0, 10)}
            Data     : ${this.data}`;
    }
}