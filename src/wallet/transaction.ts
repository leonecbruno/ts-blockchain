import { TransactionInput } from "./transaction-input";

export class Transaction {
    id: string;
    txInput: TransactionInput;
}